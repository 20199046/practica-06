﻿using System;

namespace practica06 
{
    class persona
    {
        private long cedula;
        private string nombre;
        private string apellido;
        private int edad;
        

        public persona()
        {
            Console.WriteLine("Introduzca su cedula");
            string linea = Console.ReadLine();
            cedula = long.Parse(linea);

            Console.WriteLine("Indruzca su nombre");
            nombre = Console.ReadLine();

            Console.WriteLine("Indruzca su apellido");
            apellido = Console.ReadLine();

            Console.WriteLine("Indruzca su edad");
            string año = Console.ReadLine();
            edad = int.Parse(año);
        }

        public persona(int cedula, string nombre, string apellido, int edad, int sueldo)
        {
            this.cedula = cedula;
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            
           
        }

        public void imprimir()
        {
            Console.WriteLine("Su Cedula es:" + cedula);
            Console.WriteLine("Su Nombre es:" + nombre);
            Console.WriteLine("Su Apellido es:" + apellido);
            Console.WriteLine("Su Edad es:" + edad);
            
        }
    }


    class profesor: persona
    {
        private int Sueldo;

        public profesor()
        {
            Console.WriteLine("Introducir el sueldo");
            string linea = Console.ReadLine();
            Sueldo = int.Parse(linea);
        }

        public void Mostrar()
        {
            Console.WriteLine("su sueldo es:" + Sueldo);
        }

        public profesor(int cedula, string nombre, string apellido, int edad, int _sueldo)
            : base(cedula, nombre, apellido, edad, _sueldo)
        {
        }

        static void Main(string[] args)
        {
            persona _persona = new persona();
            _persona.imprimir();
            Console.WriteLine();
            profesor _profesor = new profesor();
            _profesor.Mostrar();
            
            Console.ReadKey();
        }

    }
    
}
